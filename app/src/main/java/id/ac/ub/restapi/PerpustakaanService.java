package id.ac.ub.restapi;

import android.telecom.Call;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.


public interface PerpustakaanService {
    Call<List<Buku>> listBuku();

    Call<Buku> getBuku(@Path("id") String id);

    @FormUrlEncoded
    Call<Buku> create(@Field("judul") String judul,
                      @Field("deskripsi") String deskripsi);

}


